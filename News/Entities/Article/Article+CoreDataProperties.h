//
//  Article+CoreDataProperties.h
//  RSS
//
//  Created by Venom on 11/7/19.
//  Copyright © 2019 test. All rights reserved.
//
//

#import "Article+CoreDataClass.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Article (CoreDataProperties)

+ (NSFetchRequest<Article *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *author;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *urlImage;


@end

NS_ASSUME_NONNULL_END
