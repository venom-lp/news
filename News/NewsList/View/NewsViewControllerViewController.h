//
//  NewsViewControllerViewController.h
//  News
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsViewControllerProtocols.h"
#import "NewsViewControllerPresenter.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewsViewControllerViewController : UIViewController<NewsViewControllerViewProtocol>

@property (nonatomic) NewsViewControllerPresenter *presenter;

@end

NS_ASSUME_NONNULL_END
