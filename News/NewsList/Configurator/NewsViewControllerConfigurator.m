//
//  NewsViewControllerConfigurator.m
//  News
//
//  Created by Venom on 11/11/19.
//  Copyright © 2019 test. All rights reserved.
//

#import "NewsViewControllerConfigurator.h"
#import "NewsViewControllerRouter.h"
#import "NewsViewControllerViewController.h"
#import "NewsViewControllerInteractor.h"
#import "NewsViewControllerPresenter.h"
#import "CoreDataManager.h"
#import "DownloadImageService.h"
#import "ImageInteractorService.h"

@implementation NewsViewControllerConfigurator

+ (UIViewController *)createModule
{
    NewsViewControllerViewController *viewController = [[NewsViewControllerViewController alloc] init];
    NewsViewControllerInteractor *interactor = [[NewsViewControllerInteractor alloc] init];
    NewsViewControllerRouter *router = [[NewsViewControllerRouter alloc] initWithViewController:viewController];
    NSManagedObjectContext * context = [CoreDataManager.shared.persistentContainer newBackgroundContext];
    DownloadImageService *downloadService = [[DownloadImageService alloc]initWithDelegate:interactor];
    ImageInteractorService * imageService = [[ImageInteractorService alloc]initWith:downloadService];
    NewsViewControllerPresenter *presenter = [[NewsViewControllerPresenter alloc] initWithInterface:viewController interactor:interactor router:router backgroundContext:context imageDownloadService:imageService];
    viewController.presenter = presenter;
    return viewController;
}


@end
