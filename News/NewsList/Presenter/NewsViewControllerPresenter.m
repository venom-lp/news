//
//  NewsViewControllerPresenter.m
//  News
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import "NewsViewControllerPresenter.h"
#import "ImageInteractorService.h"
#import "Article+CoreDataClass.h"

@interface NewsViewControllerPresenter()

@property (nonatomic, strong, readonly) ImageInteractorService * imageServiceInteractor;
@property (nonatomic, strong) NSArray<Article*>* articles;
@end

@implementation NewsViewControllerPresenter

- (instancetype)initWithInterface:(id<NewsViewControllerViewProtocol>)interface
                       interactor:(id<NewsViewControllerInteractorInputProtocol>)interactor
                           router:(id<NewsViewControllerWireframeProtocol>)router
                backgroundContext:(NSManagedObjectContext*)context
             imageDownloadService:(ImageInteractorService*)service {

    if (self = [super init])
    {
        _view = interface;
        _interactor = interactor;
        _router = router;
        _imageServiceInteractor = service;
        [_interactor setPresenter:self];
        [_interactor setBackgroundContext:context];
    }
    return self;
}


- (void)loadNews {
    [self.interactor retrieveNews];
}

-(UIImage*)articleImageWithPath:(NSString*)path {
   return [self.imageServiceInteractor getImage:path];
}

- (void)downloadImage:(NSString *)urlpath {
    [self.imageServiceInteractor startDownload:urlpath];
}

-(void)saveImage:(NSString*)path image:(NSURL*)urlImage {
    [self.imageServiceInteractor saveImage:path image:urlImage];

    NSMutableArray<NSIndexPath*>* indexPathes = [[NSMutableArray alloc] init];
    [self.articles enumerateObjectsUsingBlock:^(Article * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.urlImage isEqualToString:path]) {
            [indexPathes addObject:[NSIndexPath indexPathForRow:idx inSection:0]];
        }
    }];
    [self.view reloadArticle:indexPathes];
}

- (void)receivedNews:(NSArray<Article*>*)articles {
    self.articles = articles;
    if (self.articles.count > 0) {
        [self.view showNews];
    } else {
#warning TODO : show Empty placeholder
    }
}

- (void)onError:(NSError *)error { 
    [self.view showError:error];
}




@end
