//
//  BasicCommand.m
//  RSS
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import "BasicCommand.h"

@implementation BasicCommand

-(BOOL)isMocked {
    return false;
}

-(NSDictionary*)parameters {
    return @{};
}
-(HttpMethod)httpMethod {
    return HttpMethodGET;
}

-(NSString*)method {
    NSAssert(NO, @"need override method name");
    return nil;
}

-(NSString*)baseUrlString {
    return @"https://newsapi.org/v2/";
}

-(NSString*)fullRequestString {
    return [NSString stringWithFormat:@"%@%@", self.baseUrlString, self.method];
}

-(NSURLRequest*)requestWithParams {
    
    NSURLComponents * components = [[NSURLComponents alloc]initWithString:self.fullRequestString];
    NSMutableURLRequest * request;
    if (self.httpMethod == HttpMethodGET) {
        NSMutableArray* urlQueryItems = [[NSMutableArray alloc]init];
        for (NSString* key in self.parameters) {
            NSURLQueryItem * item = [[NSURLQueryItem alloc]initWithName:key value:[self.parameters[key] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
            [urlQueryItems addObject:item];
        }
        components.queryItems = urlQueryItems;
        request = [[NSMutableURLRequest alloc]initWithURL:components.URL];
        [request setHTTPMethod:@"GET"];

    } else {
#warning TODO: init Post method
    }
    return request;
}


-(void)request:(ApiCompletionBlock)completion {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[NSURLSession.sharedSession dataTaskWithRequest:self.requestWithParams completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(response, data, error);
            });
        }] resume];
    });
}

@end
