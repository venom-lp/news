//
//  NewsViewControllerInteractor.m
//  News
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import "NewsViewControllerInteractor.h"
#import "NewsFeedCommand.h"
#import "CoreDataManager.h"
#import "Article+CoreDataProperties.h"
#import "Article+CoreDataClass.h"


@interface NewsViewControllerInteractor()

@property (nonatomic, strong, readonly) NSManagedObjectContext * backgroundContext;

@end

@implementation NewsViewControllerInteractor

#pragma mark - InteractorProtocol

- (void)setPresenter:(id<NewsViewControllerInteractorOutputProtocol>)presenter
{
    _presenter = presenter;
}

- (id<NewsViewControllerInteractorOutputProtocol>)getPresenter
{
    return self.presenter;
}

-(void)setBackgroundContext:(NSManagedObjectContext *)backgroundContext
{
    _backgroundContext = backgroundContext;
    _backgroundContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
    _backgroundContext.undoManager = nil;
}

-(NSManagedObjectContext*)getBackgroundContext
{
    return self.backgroundContext;
}

- (void)retrieveNews
{
    [[[NewsFeedCommand alloc] initWithQuery:@"bitcoin"] executeFeedWithCompletion:^(NSArray* feed) {
        [self syncArticles:feed];
    } andError:^(NSError* error) {
        [self.presenter onError:error];
    }];
}

-(void)syncArticles:(NSArray*)newArticles
{
    [Article syncArticles:newArticles inManagedObjectContext:self.backgroundContext withCompletion:^(NSArray<Article *> * _Nonnull articles) {
        [self.presenter receivedNews:articles];
    }];
}

- (void)URLSession:(nonnull NSURLSession *)session downloadTask:(nonnull NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(nonnull NSURL *)location {
    [self.presenter saveImage:downloadTask.originalRequest.URL.absoluteString image:location];
}

@end
