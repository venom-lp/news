//
//  RSSFeedCommand.m
//  RSS
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import "NewsFeedCommand.h"

@implementation NewsFeedCommand

-(NSString *)method {
    return @"everything";
}
- (NSDictionary *)parameters {
    return @{@"apiKey": @"1b86351199124e8d8d8207ca7d17f940", @"q":_query};
}

-(instancetype)initWithQuery:(NSString*)query {
    if (self = [super init]) {
        _query = [query copy];
    }
    return self;
}

-(void)executeFeedWithCompletion:(void (^)(NSArray* articles))completion andError:(void (^)(NSError* error))error {
    [self request:^(NSURLResponse * _Nonnull response, NSData * _Nonnull data, NSError * _Nonnull connectionError) {
        if (connectionError != nil) {
            error(connectionError);
        } else {
            NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
          //  NSLog(@"JSON: %@", json);
            completion(json[@"articles"]);
        }
    }];
}

@end
