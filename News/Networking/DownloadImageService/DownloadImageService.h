//
//  DownloadImageService.h
//  News
//
//  Created by Venom on 11/11/19.
//  Copyright © 2019 test. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DownloadImageService : NSObject

@property (nonatomic, strong, readonly) NSMutableSet<NSString*>* activeDownloads;

-(instancetype)initWithDelegate:(id<NSURLSessionDelegate>)delegate;
-(void)startDownload:(NSString*)urlPath;

@end

NS_ASSUME_NONNULL_END
