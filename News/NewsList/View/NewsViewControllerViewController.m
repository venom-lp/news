//
//  NewsViewControllerViewController.m
//  News
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import "NewsViewControllerViewController.h"
#import "ArticleTableViewCell.h"
#import "UITableViewCell+ReuseIdentifier.h"
#import "Article+CoreDataClass.h"
#import "NSObject+NullValidate.h"

@interface NewsViewControllerViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView* newsTableView;

@end

@implementation NewsViewControllerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureTableView];
    [self.presenter loadNews];
}

-(void)configureTableView
{
    self.view.backgroundColor = [UIColor whiteColor];
    self.newsTableView = [[UITableView alloc]initWithFrame:self.view.frame];
    self.newsTableView.delegate = self;
    self.newsTableView.dataSource = self;
    self.newsTableView.hidden = true;
    [self.newsTableView registerClass:[ArticleTableViewCell class] forCellReuseIdentifier:ArticleTableViewCell.reuseIdentifier];
    [self.view addSubview:self.newsTableView];
}

-(void)showNews
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.newsTableView.hidden = false;
        [self.newsTableView reloadData];
    });

}

-(void)reloadArticle:(NSArray<NSIndexPath*>*)indexPath {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.newsTableView reloadRowsAtIndexPaths:indexPath withRowAnimation: UITableViewRowAnimationAutomatic];
    });
}

#warning TODO : Show error
-(void)showError:(NSError*)error;
{
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.presenter.articles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ArticleTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:ArticleTableViewCell.reuseIdentifier];
    Article * article = self.presenter.articles[indexPath.row];
    NSString *articleUrl = article.urlImage;
    if (![articleUrl isNullorEmpty]) {
        UIImage * image = [self.presenter articleImageWithPath:article.urlImage];
        if (image) {
            [cell updateImage:image];
        } else {
#warning TODO : add placeholder method to cell
            [self.presenter downloadImage:article.urlImage];
        }
    }
    [cell configureArticle:self.presenter.articles[indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ArticleTableViewCell.cellHeight;
}

@end
