//
//  Article+CoreDataClass.m
//  RSS
//
//  Created by Venom on 11/7/19.
//  Copyright © 2019 test. All rights reserved.
//
//

#import "Article+CoreDataClass.h"
#import "NSManagedObject+EntityName.h"
#import "NSObject+NullValidate.h"

@implementation Article

+ (instancetype)insertItemWithWithDictionary:(NSDictionary*)dictionary
                      inManagedObjectContext:(NSManagedObjectContext *)managedObjectContext
{
    Article* item = [NSEntityDescription insertNewObjectForEntityForName:self.entityName
                                               inManagedObjectContext:managedObjectContext];

    item.title = [dictionary[@"title"] nullToEmpty];
    item.urlImage = [dictionary[@"urlToImage"] nullToEmpty];
    item.author = [dictionary[@"author"] nullToEmpty];
    
    return item;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@,%@", self.title, self.author];
}

#warning TODO : add Error completion
+ (void)syncArticles:(NSArray*)newArticles
inManagedObjectContext:(NSManagedObjectContext*)managedObjectContext
              withCompletion:(void (^)(NSArray<Article*>* articles))completion {

    [managedObjectContext performBlock:^{

        NSError * error;
        NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:[Article fetchRequest] error:&error];
        for (Article* article in fetchedObjects) {
            [managedObjectContext deleteObject:article];
        }

        for (NSDictionary* articleDictionary in newArticles ) {
            [Article insertItemWithWithDictionary:articleDictionary inManagedObjectContext:managedObjectContext];
        }

        if (managedObjectContext.hasChanges) {
            [managedObjectContext save:&error];
            [managedObjectContext reset];
        }
        NSArray *articles = [managedObjectContext executeFetchRequest:[Article fetchRequest] error:nil];
        completion(articles);
    }];
}

@end
