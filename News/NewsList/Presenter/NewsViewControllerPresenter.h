//
//  NewsViewControllerPresenter.h
//  News
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsViewControllerProtocols.h"

@class ImageInteractorService;
@class Article;

NS_ASSUME_NONNULL_BEGIN

@interface NewsViewControllerPresenter : NSObject<NewsViewControllerInteractorOutputProtocol, NewsViewControllerPresenterProtocol>

@property (nonatomic, weak, nullable) id<NewsViewControllerViewProtocol> view;
@property (nonatomic) id<NewsViewControllerInteractorInputProtocol> interactor;
@property (nonatomic) id<NewsViewControllerWireframeProtocol> router;

- (instancetype)initWithInterface:(id<NewsViewControllerViewProtocol>)interface
                       interactor:(id<NewsViewControllerInteractorInputProtocol>)interactor
                           router:(id<NewsViewControllerWireframeProtocol>)router
                backgroundContext:(NSManagedObjectContext*)context
                imageDownloadService:(ImageInteractorService*)service;

@end

NS_ASSUME_NONNULL_END
