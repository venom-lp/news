//
//  NSObject+NullValidate.h
//  News
//
//  Created by Venom on 11/12/19.
//  Copyright © 2019 test. All rights reserved.
//


#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (NullValidate)

-(NSString*)nullToEmpty;
- (BOOL)isNullorEmpty;

@end

NS_ASSUME_NONNULL_END
