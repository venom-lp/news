//
//  NSManagedObject+EntityName.m
//  RSS
//
//  Created by Venom on 11/7/19.
//  Copyright © 2019 test. All rights reserved.
//

#import "NSManagedObject+EntityName.h"


@implementation NSManagedObject (EntityName)

+ (NSString *)entityName {
    return NSStringFromClass([self class]);
}

@end
