//
//  DownloadImageService.m
//  News
//
//  Created by Venom on 11/11/19.
//  Copyright © 2019 test. All rights reserved.
//

#import "ImageInteractorService.h"
#import "DownloadImageService.h"

@interface ImageInteractorService()

@property (nonatomic, strong, readonly) DownloadImageService * service;
@property (nonatomic, strong, readonly) NSCache * imagesCache;

@end

@implementation ImageInteractorService

-(instancetype)initWith:(DownloadImageService*)service {
    if (self = [super init]) {
        _imagesCache =  [[NSCache alloc] init];
        _service = service;
    }
    return self;
}


-(UIImage*)getImage:(NSString*)key {
   return [self.imagesCache objectForKey:key];
}

-(void)startDownload:(NSString*)urlPath {
    if (![self.service.activeDownloads containsObject:urlPath]) {
        [self.service startDownload:urlPath];
    }
}

-(void)saveImage:(NSString*)path image:(NSURL*)urlImage {
    NSData *data = [NSData dataWithContentsOfURL:urlImage];
    UIImage * image = [UIImage imageWithData:data];
    if (image) {
        [self.imagesCache setObject:image forKey:path];
        [self.service.activeDownloads removeObject:path];
    }
}


@end
