//
//  RSSTableViewCell.m
//  RSS
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import "ArticleTableViewCell.h"
#import "Article+CoreDataProperties.h"

@interface ArticleTableViewCell()

@property (nonatomic, strong) UILabel *authorLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *articleImageView;

@end

@implementation ArticleTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self != nil) {
        self.selectionStyle = UITableViewCellSeparatorStyleNone;
        [self setupTitleLabel];
        [self setupAuthorLabel];
        [self setupImageView];
    }
    
    return self;
}

-(void)setupTitleLabel
{
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    _titleLabel.numberOfLines = 0;
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.translatesAutoresizingMaskIntoConstraints = false;

    [self.contentView addSubview: _titleLabel];

    [_titleLabel.topAnchor constraintEqualToAnchor:self.contentView.topAnchor constant:10.0].active = true;
    [_titleLabel.centerXAnchor constraintEqualToAnchor:self.contentView.centerXAnchor].active = true;
    [_titleLabel.widthAnchor constraintEqualToConstant:self.contentView.frame.size.width - 50].active = true;

}

-(void)setupAuthorLabel
{
    _authorLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    _authorLabel.translatesAutoresizingMaskIntoConstraints = false;

    [self.contentView addSubview: _authorLabel];

    [_authorLabel.topAnchor constraintEqualToAnchor:_titleLabel.bottomAnchor constant:20.0].active = true;
    [_authorLabel.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor constant:10].active = true;
}

-(void)setupImageView
{
    _articleImageView = [[UIImageView alloc]initWithFrame:CGRectZero];
    _articleImageView.backgroundColor = [UIColor lightGrayColor];
    _articleImageView.contentMode = UIViewContentModeScaleAspectFill;
    _articleImageView.translatesAutoresizingMaskIntoConstraints = false;

    [self.contentView addSubview: _articleImageView];

    [_articleImageView.topAnchor constraintEqualToAnchor:_authorLabel.bottomAnchor constant:10].active = true;
    [_articleImageView.leftAnchor constraintEqualToAnchor:self.contentView.leftAnchor].active = true;
    [_articleImageView.rightAnchor constraintEqualToAnchor:self.contentView.rightAnchor].active = true;
    [_articleImageView.bottomAnchor constraintEqualToAnchor:self.contentView.bottomAnchor].active = true;
}

-(void)configureArticle:(Article*)article {
    self.authorLabel.text = article.author;
    self.titleLabel.text = article.title;
}

-(void)updateImage:(UIImage*)image {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.articleImageView.image = image;
    });
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    _articleImageView.image = nil;
}

+ (CGFloat)cellHeight
{
    return 500.0;
}

@end
