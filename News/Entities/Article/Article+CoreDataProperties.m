//
//  Article+CoreDataProperties.m
//  RSS
//
//  Created by Venom on 11/7/19.
//  Copyright © 2019 test. All rights reserved.
//
//

#import "Article+CoreDataProperties.h"
#import "NSManagedObject+EntityName.h"

@implementation Article (CoreDataProperties)

+ (NSFetchRequest<Article *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:self.entityName];
}

@dynamic author;
@dynamic title;
@dynamic urlImage;

@end
