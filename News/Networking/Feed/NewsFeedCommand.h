//
//  RSSFeedCommand.h
//  RSS
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import "BasicCommand.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewsFeedCommand : BasicCommand

@property (nonatomic, readonly, copy) NSString* query;

-(instancetype)initWithQuery:(NSString*)query;

-(void)executeFeedWithCompletion:(void (^)(NSArray* feed))completion andError:(void (^)(NSError* error))error;

@end

NS_ASSUME_NONNULL_END
