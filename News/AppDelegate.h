//
//  AppDelegate.h
//  RSS
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

