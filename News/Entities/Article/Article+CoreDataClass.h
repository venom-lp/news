//
//  Article+CoreDataClass.h
//  RSS
//
//  Created by Venom on 11/7/19.
//  Copyright © 2019 test. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Article : NSManagedObject
+ (void)syncArticles:(NSArray*)newArticles
      inManagedObjectContext:(NSManagedObjectContext*)managedObjectContext
              withCompletion:(void (^)(NSArray<Article*>* articles))completion ;

@end

NS_ASSUME_NONNULL_END

#import "Article+CoreDataProperties.h"
