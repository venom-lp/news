//
//  NewsViewControllerRouter.h
//  News
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewsViewControllerProtocols.h"
#import "NewsViewControllerViewController.h"

@interface NewsViewControllerRouter : NSObject<NewsViewControllerWireframeProtocol>

- (instancetype)initWithViewController:(NewsViewControllerViewController*)controller;

@end
