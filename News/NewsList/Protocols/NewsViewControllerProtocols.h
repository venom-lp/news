//
//  NewsViewControllerProtocols.h
//  News
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@class Article;


#pragma mark - WireFrameProtocol

@protocol NewsViewControllerWireframeProtocol <NSObject>

@end

#pragma mark - PresenterProtocol

@protocol NewsViewControllerPresenterProtocol <NSObject>

-(NSArray<Article*>*)articles;

-(void)loadNews;
-(void)downloadImage:(NSString*)urlpath;
-(UIImage*)articleImageWithPath:(NSString*)path;

@end

#pragma mark - InteractorProtocol

@protocol NewsViewControllerInteractorOutputProtocol <NSObject>

/** Interactor -> Presenter */
-(void)saveImage:(NSString*)path image:(NSURL*)urlImage;
-(void)receivedNews:(NSArray<Article*>*)articles;
-(void)onError:(NSError*)error;

@end

@protocol NewsViewControllerInteractorInputProtocol <NSObject>

/** Presenter -> Interactor */

- (void)setPresenter:(id<NewsViewControllerInteractorOutputProtocol>)presenter;
- (id<NewsViewControllerInteractorOutputProtocol>)getPresenter;

-(void)setBackgroundContext:(NSManagedObjectContext *)backgroundContext;
-(NSManagedObjectContext*)getBackgroundContext;

-(void)retrieveNews;

@end

#pragma mark - ViewProtocol

@protocol NewsViewControllerViewProtocol <NSObject>

/** Presenter -> ViewController */

#warning TODO: add HUD methods
-(void)showNews;
-(void)showError:(NSError*)error;
-(void)reloadArticle:(NSArray<NSIndexPath*>*)indexPath;

@end
