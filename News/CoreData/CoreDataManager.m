//
//  CoreDataManager.m
//  RSS
//
//  Created by Venom on 11/7/19.
//  Copyright © 2019 test. All rights reserved.
//

#import "CoreDataManager.h"

@interface CoreDataManager ()

@property (strong, nonatomic, readwrite) NSPersistentContainer *persistentContainer;

@end

@implementation CoreDataManager
#pragma mark - singleton

+ (instancetype)shared
{
    static dispatch_once_t pred;
    static CoreDataManager *sharedCoreDataManager = nil;
    dispatch_once(&pred, ^{
        sharedCoreDataManager = [[self alloc] init];
    });
    return sharedCoreDataManager;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"News"];
        [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription * description, NSError * error) {
        }];
        _persistentContainer.viewContext.automaticallyMergesChangesFromParent = true;
        _persistentContainer.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy;
        _persistentContainer.viewContext.undoManager = nil;
        _persistentContainer.viewContext.shouldDeleteInaccessibleFaults = true;
    }
    return self;
}

@end
