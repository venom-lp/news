//
//  NewsViewControllerRouter.m
//  News
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//
 
#import "NewsViewControllerRouter.h"
#import "NewsViewControllerViewController.h"
#import "NewsViewControllerInteractor.h"
#import "NewsViewControllerPresenter.h"

@interface NewsViewControllerRouter()

@property (nonatomic, weak) NewsViewControllerViewController *viewController;

@end
@implementation NewsViewControllerRouter

- (instancetype)initWithViewController:(NewsViewControllerViewController *)controller
{
    if ((self = [super init]))
    {
        _viewController = controller;
    }
    return self;
}

 
@end
