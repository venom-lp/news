//
//  BasicCommand.h
//  RSS
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BasicCommand : NSObject

typedef void (^ApiCompletionBlock) (NSURLResponse * response, NSData * data, NSError * connectionError);

typedef NS_ENUM(NSInteger , HttpMethod)
{
    HttpMethodGET = 0,
    HttpMethodPOST
};

-(BOOL)isMocked;
-(NSDictionary*)parameters;
-(NSString*)method;
-(HttpMethod)httpMethod;
-(void)request:(ApiCompletionBlock)completion;

@end

NS_ASSUME_NONNULL_END
