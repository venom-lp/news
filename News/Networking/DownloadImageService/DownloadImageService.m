//
//  DownloadImageService.m
//  News
//
//  Created by Venom on 11/11/19.
//  Copyright © 2019 test. All rights reserved.
//

#import "DownloadImageService.h"

@interface DownloadImageService ()

@property (nonatomic, strong) NSURLSession * urlSession;
@property (nonatomic, weak) id<NSURLSessionDelegate> delegate;

@end

@implementation DownloadImageService

- (instancetype)initWithDelegate:(id<NSURLSessionDelegate>)delegate {

    if (self = [super init]) {
        _delegate = delegate;
        _activeDownloads = [[NSMutableSet alloc]init];
    }
    return self;
}

-(NSURLSession *)urlSession {
    if (!_urlSession) {
        NSURLSessionConfiguration *backgroundConfiguration = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:@"backgroundSession"];
        _urlSession = [NSURLSession sessionWithConfiguration:backgroundConfiguration delegate:self.delegate delegateQueue:nil];
    }
    return _urlSession;
}

- (void)startDownload:(NSString *)urlPath {
    [[self.urlSession downloadTaskWithURL:[NSURL URLWithString:urlPath]] resume];
    [self.activeDownloads addObject:urlPath];
}

@end
