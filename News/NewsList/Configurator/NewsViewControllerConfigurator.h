//
//  NewsViewControllerConfigurator.h
//  News
//
//  Created by Venom on 11/11/19.
//  Copyright © 2019 test. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NewsViewControllerConfigurator : NSObject

+ (UIViewController *)createModule;

@end

NS_ASSUME_NONNULL_END
