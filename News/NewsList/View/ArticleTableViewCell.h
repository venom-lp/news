//
//  RSSTableViewCell.h
//  RSS
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Article;
NS_ASSUME_NONNULL_BEGIN

@interface ArticleTableViewCell : UITableViewCell

@property (nonatomic, strong, readonly) UILabel *authorLabel;
@property (nonatomic, strong, readonly) UILabel *titleLabel;
@property (nonatomic, strong, readonly) UIImageView *articleImageView;

-(void)updateImage:(UIImage*)image;
-(void)configureArticle:(Article*)article;

+(CGFloat)cellHeight;

@end

NS_ASSUME_NONNULL_END
