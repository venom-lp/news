//
//  NSObject+NullValidate.m
//  News
//
//  Created by Venom on 11/12/19.
//  Copyright © 2019 test. All rights reserved.
//

#import "NSObject+NullValidate.h"

@implementation NSObject (NullValidate)

-(id)nullToEmpty {
    return [self isNullorEmpty] ? @"" : self;
}

- (BOOL)isNullorEmpty {
    return self == [NSNull null] || [self isEqual:@""] || self == nil;
}

@end
