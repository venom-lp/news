//
//  DownloadImageService.h
//  News
//
//  Created by Venom on 11/11/19.
//  Copyright © 2019 test. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DownloadImageService;

NS_ASSUME_NONNULL_BEGIN

@interface ImageInteractorService : NSObject

-(UIImage*)getImage:(NSString*)key;
-(void)startDownload:(NSString*)urlPath;
-(void)saveImage:(NSString*)path image:(NSURL*)urlImage;

-(instancetype)initWith:(DownloadImageService*)service;

@end

NS_ASSUME_NONNULL_END
