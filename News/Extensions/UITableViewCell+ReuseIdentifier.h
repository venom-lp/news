//
//  UITableViewCell+ReuseIdentifier.h
//  RSS
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITableViewCell (ReuseIdentifier)

+ (NSString *)reuseIdentifier;


@end

NS_ASSUME_NONNULL_END
