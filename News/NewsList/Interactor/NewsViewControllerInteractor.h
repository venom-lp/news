//
//  NewsViewControllerInteractor.h
//  News
//
//  Created by Venom on 07.11.2019.
//  Copyright © 2019 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "NewsViewControllerProtocols.h"

NS_ASSUME_NONNULL_BEGIN

@interface NewsViewControllerInteractor : NSObject<NewsViewControllerInteractorInputProtocol,NSURLSessionDownloadDelegate>

@property (nonatomic, weak, nullable) id<NewsViewControllerInteractorOutputProtocol> presenter;

@end

NS_ASSUME_NONNULL_END
