//
//  CoreDataManager.h
//  RSS
//
//  Created by Venom on 11/7/19.
//  Copyright © 2019 test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoreDataManager : NSObject

@property (strong, nonatomic, readonly) NSPersistentContainer *persistentContainer;

+ (instancetype)shared;

- (instancetype)init __attribute__((unavailable("Use +[CoreDataManager shared] instead")));
+ (instancetype)new __attribute__((unavailable("Use +[CoreDataManager shared] instead")));

@end

NS_ASSUME_NONNULL_END
