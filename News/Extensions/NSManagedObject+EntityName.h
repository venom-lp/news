//
//  NSManagedObject+EntityName.h
//  RSS
//
//  Created by Venom on 11/7/19.
//  Copyright © 2019 test. All rights reserved.
//

#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSManagedObject (EntityName)

+ (NSString *)entityName;

@end

NS_ASSUME_NONNULL_END
